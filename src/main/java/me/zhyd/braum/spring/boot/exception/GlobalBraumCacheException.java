package me.zhyd.braum.spring.boot.exception;


public class GlobalBraumCacheException extends RuntimeException {
    public GlobalBraumCacheException() {
        super();
    }

    public GlobalBraumCacheException(String message) {
        super(message);
    }

    public GlobalBraumCacheException(String message, Throwable cause) {
        super(message, cause);
    }

    public GlobalBraumCacheException(Throwable cause) {
        super(cause);
    }

    protected GlobalBraumCacheException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
